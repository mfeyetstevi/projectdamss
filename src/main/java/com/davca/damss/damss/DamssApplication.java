package com.davca.damss.damss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DamssApplication {

	public static void main(String[] args) {
		SpringApplication.run(DamssApplication.class, args);
	}
}
