/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import com.davca.damss.damss.entity.Classe;
import com.davca.damss.damss.entity.AnneeAcademique;
import com.davca.damss.damss.entity.Modalite;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "modalite_classe")
@XmlRootElement

public class ModaliteClasse implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_modalite_classe")
    private Integer idModaliteClasse;
    @JoinColumn(name = "annee_academique_id_annee-academique", referencedColumnName = "id_annee_academique")
    @ManyToOne(optional = false)
    private AnneeAcademique anneeAcademiqueIdAnneeAcademique;
    @JoinColumn(name = "classe_idclasse", referencedColumnName = "idclasse")
    @ManyToOne(optional = false)
    private Classe classeIdclasse;
    @JoinColumn(name = "modalite_idmodalite", referencedColumnName = "idmodalite")
    @ManyToOne(optional = false)
    private Modalite modaliteIdmodalite;

    public ModaliteClasse() {
    }

    public ModaliteClasse(Integer idModaliteClasse) {
        this.idModaliteClasse = idModaliteClasse;
    }

    public Integer getIdModaliteClasse() {
        return idModaliteClasse;
    }

    public void setIdModaliteClasse(Integer idModaliteClasse) {
        this.idModaliteClasse = idModaliteClasse;
    }

    public AnneeAcademique getAnneeAcademiqueIdAnneeAcademique() {
        return anneeAcademiqueIdAnneeAcademique;
    }

    public void setAnneeAcademiqueIdAnneeAcademique(AnneeAcademique anneeAcademiqueIdAnneeAcademique) {
        this.anneeAcademiqueIdAnneeAcademique = anneeAcademiqueIdAnneeAcademique;
    }

    public Classe getClasseIdclasse() {
        return classeIdclasse;
    }

    public void setClasseIdclasse(Classe classeIdclasse) {
        this.classeIdclasse = classeIdclasse;
    }

    public Modalite getModaliteIdmodalite() {
        return modaliteIdmodalite;
    }

    public void setModaliteIdmodalite(Modalite modaliteIdmodalite) {
        this.modaliteIdmodalite = modaliteIdmodalite;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idModaliteClasse != null ? idModaliteClasse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ModaliteClasse)) {
            return false;
        }
        ModaliteClasse other = (ModaliteClasse) object;
        if ((this.idModaliteClasse == null && other.idModaliteClasse != null) || (this.idModaliteClasse != null && !this.idModaliteClasse.equals(other.idModaliteClasse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.ModaliteClasse[ idModaliteClasse=" + idModaliteClasse + " ]";
    }
    
}
