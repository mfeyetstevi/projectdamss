/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "profil_utilisateur")
public class ProfilUtilisateur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idprofil_utilisateur")
    private Long idprofilUtilisateur;
    @JoinColumn(name = "profil_idprofil", referencedColumnName = "idprofil")
    @ManyToOne(optional = false)
    private Profil profilIdprofil;
    @JoinColumn(name = "utilisateur_idutilisateur", referencedColumnName = "idutilisateur")
    @ManyToOne(optional = false)
        private Utilisateur utilisateurIdutilisateur;

    public ProfilUtilisateur() {
    }

    public ProfilUtilisateur(Long idprofilUtilisateur) {
        this.idprofilUtilisateur = idprofilUtilisateur;
    }

    public Long getIdprofilUtilisateur() {
        return idprofilUtilisateur;
    }

    public void setIdprofilUtilisateur(Long idprofilUtilisateur) {
        this.idprofilUtilisateur = idprofilUtilisateur;
    }

    public Profil getProfilIdprofil() {
        return profilIdprofil;
    }

    public void setProfilIdprofil(Profil profilIdprofil) {
        this.profilIdprofil = profilIdprofil;
    }

    public Utilisateur getUtilisateurIdutilisateur() {
        return utilisateurIdutilisateur;
    }

    public void setUtilisateurIdutilisateur(Utilisateur utilisateurIdutilisateur) {
        this.utilisateurIdutilisateur = utilisateurIdutilisateur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idprofilUtilisateur != null ? idprofilUtilisateur.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProfilUtilisateur)) {
            return false;
        }
        ProfilUtilisateur other = (ProfilUtilisateur) object;
        if ((this.idprofilUtilisateur == null && other.idprofilUtilisateur != null) || (this.idprofilUtilisateur != null && !this.idprofilUtilisateur.equals(other.idprofilUtilisateur))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return profilIdprofil.getIntitule();
    }
}
