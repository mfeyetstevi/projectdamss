/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
public class Profil implements Serializable {
private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idprofil")
    private Long idprofil;
    @Size(max = 45)
    @Column(name = "intitule")
    private String intitule;
    
    
    @Size(max = 45)
    @Column(name = "description")
    private String description;
    
    @Size(max = 45)
    @Column(name = "statut")
    private String statut;
    
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profilIdprofil")
    private List<ProfilUtilisateur> profilUtilisateurList;

    public Profil() {
    }

    public Profil(Long idprofil) {
        this.idprofil = idprofil;
    }

    public Long getIdprofil() {
        return idprofil;
    }

    public void setIdprofil(Long idprofil) {
        this.idprofil = idprofil;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    @XmlTransient
    public List<ProfilUtilisateur> getProfilUtilisateurList() {
        return profilUtilisateurList;
    }

    public void setProfilUtilisateurList(List<ProfilUtilisateur> profilUtilisateurList) {
        this.profilUtilisateurList = profilUtilisateurList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idprofil != null ? idprofil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profil)) {
            return false;
        }
        Profil other = (Profil) object;
        if ((this.idprofil == null && other.idprofil != null) || (this.idprofil != null && !this.idprofil.equals(other.idprofil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Profil[ idprofil=" + idprofil + " ]";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }
    
    
    
}
