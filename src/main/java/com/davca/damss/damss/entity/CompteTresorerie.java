/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "compte_tresorerie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CompteTresorerie.findAll", query = "SELECT c FROM CompteTresorerie c")
    , @NamedQuery(name = "CompteTresorerie.findByIdcompteTresorerie", query = "SELECT c FROM CompteTresorerie c WHERE c.idcompteTresorerie = :idcompteTresorerie")
    , @NamedQuery(name = "CompteTresorerie.findByIntitule", query = "SELECT c FROM CompteTresorerie c WHERE c.intitule = :intitule")
    , @NamedQuery(name = "CompteTresorerie.findByDescription", query = "SELECT c FROM CompteTresorerie c WHERE c.description = :description")
    , @NamedQuery(name = "CompteTresorerie.findByNomResponsable", query = "SELECT c FROM CompteTresorerie c WHERE c.nomResponsable = :nomResponsable")})
public class CompteTresorerie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idcompte_tresorerie")
    private Long idcompteTresorerie;
    @Size(max = 45)
    @Column(name = "intitule")
    private String intitule;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    
        @Size(max = 50)
    @Column(name = "statut")
    private String statut;
    @Size(max = 40)
    @Column(name = "nom_responsable")
    private String nomResponsable;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "compteTresorerieIdcompteTresorerie")
    private List<Paiement> paiementList;

    public CompteTresorerie() {
    }

    public CompteTresorerie(Long idcompteTresorerie) {
        this.idcompteTresorerie = idcompteTresorerie;
    }

    public Long getIdcompteTresorerie() {
        return idcompteTresorerie;
    }

    public void setIdcompteTresorerie(Long idcompteTresorerie) {
        this.idcompteTresorerie = idcompteTresorerie;
    }

    public String getIntitule() {
        return intitule;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNomResponsable() {
        return nomResponsable;
    }

    public void setNomResponsable(String nomResponsable) {
        this.nomResponsable = nomResponsable;
    }

    @XmlTransient
    public List<Paiement> getPaiementList() {
        return paiementList;
    }

    public void setPaiementList(List<Paiement> paiementList) {
        this.paiementList = paiementList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idcompteTresorerie != null ? idcompteTresorerie.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CompteTresorerie)) {
            return false;
        }
        CompteTresorerie other = (CompteTresorerie) object;
        if ((this.idcompteTresorerie == null && other.idcompteTresorerie != null) || (this.idcompteTresorerie != null && !this.idcompteTresorerie.equals(other.idcompteTresorerie))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.CompteTresorerie[ idcompteTresorerie=" + idcompteTresorerie + " ]";
    }
    
}
