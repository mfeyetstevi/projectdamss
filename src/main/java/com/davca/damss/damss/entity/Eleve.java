/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author isnov-pc
 */

@Entity
@Table(name = "eleve")
public class Eleve implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idELEVE")
    private Long idELEVE;
@Size(max = 30)
    @Column(name = "matricule")
    private String matricule;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    @Size(max = 45)
    @Column(name = "prenom")
    private String prenom;
    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date dateNaissance;
    @Size(max = 30)
    @Column(name = "lieu_naissance")
    private String lieuNaissance;
    @Column(name = "sexe")
    private String sexe;
    @Size(max = 10)
    @Column(name = "classe")
    private String classe;
    @Size(max = 20)
    @Column(name = "nationnalite")
    private String nationnalite;
    @Size(max = 20)
    @Column(name = "departement_origine")
    private String departementOrigine;
    @Size(max = 40)
    @Column(name = "quartier")
    private String quartier;
    @Size(max = 45)
    @Column(name = "nom_pere")
    private String nomPere;
    @Size(max = 45)
    @Column(name = "nom_mere")
    private String nomMere;
    @Size(max = 30)
    @Column(name = "profession_pere")
    private String professionPere;
    @Size(max = 30)
    @Column(name = "profession_mere")
    private String professionMere;
    @Column(name = "telephone_pere")
    private Integer telephonePere;
    @Column(name = "telephone_mere")
    private Integer telephoneMere;
    @Column(name = "telephone_tuteur")
    private Integer telephoneTuteur;
    @Size(max = 45)
    @Column(name = "nom_urgence")
    private String nomUrgence;
    @Column(name = "telephone_urgence")
    private Integer telephoneUrgence;
    @Column(name = "annee_ancienne_inscription")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy")
    private Date anneeAncienneInscription;
    @Size(max = 10)
    @Column(name = "classe_ancienne_inscription")
    private String classeAncienneInscription;
    @Size(max = 55)
    @Column(name = "etablissement_precedent")
    private String etablissementPrecedent;
    @Column(name = "drepanocytaire")
    private String drepanocytaire;
    @Column(name = "statut_sport")
    private String statutSport;
    @Size(max = 500)
    @Column(name = "maladies_chronique")
    private String maladiesChronique;
    @Size(max = 10)
    @Column(name = "groupe_sanguin")
    private String groupeSanguin;
    @Column(name = "rhesus")
    private String rhesus;
    @Size(max = 500)
    @Column(name = "observation")
    private String observation;
    @Size(max = 45)
    @Column(name = "statut")
    private String statut;
    @Size(max = 100)
    @Column(name = "photo")
    private String photo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eleveIdeleve")
    private List<Paiement> paiementList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eleveIdeleve")
    private List<Inscription> inscriptionList;

    public Eleve() {
    }

    public Eleve(Long idELEVE) {
        this.idELEVE = idELEVE;
    }
 

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idELEVE != null ? idELEVE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eleve)) {
            return false;
        }
        Eleve other = (Eleve) object;
        if ((this.idELEVE == null && other.idELEVE != null) || (this.idELEVE != null && !this.idELEVE.equals(other.idELEVE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""+getNom()+getPrenom();
    }

    public Long getIdELEVE() {
        return idELEVE;
    }

    public void setIdELEVE(Long idELEVE) {
        this.idELEVE = idELEVE;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getNationnalite() {
        return nationnalite;
    }

    public void setNationnalite(String nationnalite) {
        this.nationnalite = nationnalite;
    }

    public String getDepartementOrigine() {
        return departementOrigine;
    }

    public void setDepartementOrigine(String departementOrigine) {
        this.departementOrigine = departementOrigine;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getNomPere() {
        return nomPere;
    }

    public void setNomPere(String nomPere) {
        this.nomPere = nomPere;
    }

    public String getNomMere() {
        return nomMere;
    }

    public void setNomMere(String nomMere) {
        this.nomMere = nomMere;
    }

    public String getProfessionPere() {
        return professionPere;
    }

    public void setProfessionPere(String professionPere) {
        this.professionPere = professionPere;
    }

    public String getProfessionMere() {
        return professionMere;
    }

    public void setProfessionMere(String professionMere) {
        this.professionMere = professionMere;
    }

    public Integer getTelephonePere() {
        return telephonePere;
    }

    public void setTelephonePere(Integer telephonePere) {
        this.telephonePere = telephonePere;
    }

    public Integer getTelephoneMere() {
        return telephoneMere;
    }

    public void setTelephoneMere(Integer telephoneMere) {
        this.telephoneMere = telephoneMere;
    }

    public Integer getTelephoneTuteur() {
        return telephoneTuteur;
    }

    public void setTelephoneTuteur(Integer telephoneTuteur) {
        this.telephoneTuteur = telephoneTuteur;
    }

    public String getNomUrgence() {
        return nomUrgence;
    }

    public void setNomUrgence(String nomUrgence) {
        this.nomUrgence = nomUrgence;
    }

    public Integer getTelephoneUrgence() {
        return telephoneUrgence;
    }

    public void setTelephoneUrgence(Integer telephoneUrgence) {
        this.telephoneUrgence = telephoneUrgence;
    }

    public Date getAnneeAncienneInscription() {
        return anneeAncienneInscription;
    }

    public void setAnneeAncienneInscription(Date anneeAncienneInscription) {
        this.anneeAncienneInscription = anneeAncienneInscription;
    }

    public String getClasseAncienneInscription() {
        return classeAncienneInscription;
    }

    public void setClasseAncienneInscription(String classeAncienneInscription) {
        this.classeAncienneInscription = classeAncienneInscription;
    }

    public String getEtablissementPrecedent() {
        return etablissementPrecedent;
    }

    public void setEtablissementPrecedent(String etablissementPrecedent) {
        this.etablissementPrecedent = etablissementPrecedent;
    }

    public String getDrepanocytaire() {
        return drepanocytaire;
    }

    public void setDrepanocytaire(String drepanocytaire) {
        this.drepanocytaire = drepanocytaire;
    }

    public String getStatutSport() {
        return statutSport;
    }

    public void setStatutSport(String statutSport) {
        this.statutSport = statutSport;
    }

    public String getMaladiesChronique() {
        return maladiesChronique;
    }

    public void setMaladiesChronique(String maladiesChronique) {
        this.maladiesChronique = maladiesChronique;
    }

    public String getGroupeSanguin() {
        return groupeSanguin;
    }

    public void setGroupeSanguin(String groupeSanguin) {
        this.groupeSanguin = groupeSanguin;
    }

    public String getRhesus() {
        return rhesus;
    }

    public void setRhesus(String rhesus) {
        this.rhesus = rhesus;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public List<Paiement> getPaiementList() {
        return paiementList;
    }

    public void setPaiementList(List<Paiement> paiementList) {
        this.paiementList = paiementList;
    }

    public List<Inscription> getInscriptionList() {
        return inscriptionList;
    }

    public void setInscriptionList(List<Inscription> inscriptionList) {
        this.inscriptionList = inscriptionList;
    }
    
    
    
}
