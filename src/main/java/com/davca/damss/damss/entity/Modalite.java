/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "modalite")
@XmlRootElement

public class Modalite implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idmodalite")
    private Integer idmodalite;
    @Size(max = 35)
    @Column(name = "intitule")
    private String intitule;
    @Size(max = 150)
    @Column(name = "description")
    private String description;
    @Column(name = "annee_academique")
    @Temporal(TemporalType.DATE)
    private Date anneeAcademique;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "frais_inscription")
    private Float fraisInscription;
    @Column(name = "montant")
    private Float montant;
    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    @Size(max = 20)
    @Column(name = "mode_paiement")
    private String modePaiement;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "modaliteIdmodalite")
    private List<ModaliteClasse> modaliteClasseList;

    public Modalite() {
    }

    public Modalite(Integer idmodalite) {
        this.idmodalite = idmodalite;
    }

    public Integer getIdmodalite() {
        return idmodalite;
    }

    public void setIdmodalite(Integer idmodalite) {
        this.idmodalite = idmodalite;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getAnneeAcademique() {
        return anneeAcademique;
    }

    public void setAnneeAcademique(Date anneeAcademique) {
        this.anneeAcademique = anneeAcademique;
    }

    public Float getFraisInscription() {
        return fraisInscription;
    }

    public void setFraisInscription(Float fraisInscription) {
        this.fraisInscription = fraisInscription;
    }

    public Float getMontant() {
        return montant;
    }

    public void setMontant(Float montant) {
        this.montant = montant;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(String modePaiement) {
        this.modePaiement = modePaiement;
    }

    @XmlTransient
    public List<ModaliteClasse> getModaliteClasseList() {
        return modaliteClasseList;
    }

    public void setModaliteClasseList(List<ModaliteClasse> modaliteClasseList) {
        this.modaliteClasseList = modaliteClasseList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idmodalite != null ? idmodalite.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modalite)) {
            return false;
        }
        Modalite other = (Modalite) object;
        if ((this.idmodalite == null && other.idmodalite != null) || (this.idmodalite != null && !this.idmodalite.equals(other.idmodalite))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.Modalite[ idmodalite=" + idmodalite + " ]";
    }
    
}