/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "annee_academique")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AnneeAcademique.findAll", query = "SELECT a FROM AnneeAcademique a")
    , @NamedQuery(name = "AnneeAcademique.findByIdAnneeAcademique", query = "SELECT a FROM AnneeAcademique a WHERE a.idAnneeAcademique = :idAnneeAcademique")
    , @NamedQuery(name = "AnneeAcademique.findByLibelle", query = "SELECT a FROM AnneeAcademique a WHERE a.libelle = :libelle")
    , @NamedQuery(name = "AnneeAcademique.findByDateDebut", query = "SELECT a FROM AnneeAcademique a WHERE a.dateDebut = :dateDebut")
    , @NamedQuery(name = "AnneeAcademique.findByDateFin", query = "SELECT a FROM AnneeAcademique a WHERE a.dateFin = :dateFin")
    , @NamedQuery(name = "AnneeAcademique.findByStatus", query = "SELECT a FROM AnneeAcademique a WHERE a.status = :status")})
public class AnneeAcademique implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_annee_academique")
    private Integer idAnneeAcademique;
    @Size(max = 45)
    @Column(name = "libelle")
    private String libelle;
    @Column(name = "date_debut")
    @Temporal(TemporalType.DATE)
    private Date dateDebut;
    @Column(name = "date_fin")
    @Temporal(TemporalType.DATE)
    private Date dateFin;
    @Column(name = "status")
    private Boolean status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "anneeAcademiqueIdAnneeAcademique")
    private List<ModaliteClasse> modaliteClasseList;

    public AnneeAcademique() {
    }

    public AnneeAcademique(Integer idAnneeAcademique) {
        this.idAnneeAcademique = idAnneeAcademique;
    }

    public Integer getIdAnneeAcademique() {
        return idAnneeAcademique;
    }

    public void setIdAnneeAcademique(Integer idAnneeAcademique) {
        this.idAnneeAcademique = idAnneeAcademique;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    @XmlTransient
    public List<ModaliteClasse> getModaliteClasseList() {
        return modaliteClasseList;
    }

    public void setModaliteClasseList(List<ModaliteClasse> modaliteClasseList) {
        this.modaliteClasseList = modaliteClasseList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAnneeAcademique != null ? idAnneeAcademique.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AnneeAcademique)) {
            return false;
        }
        AnneeAcademique other = (AnneeAcademique) object;
        if ((this.idAnneeAcademique == null && other.idAnneeAcademique != null) || (this.idAnneeAcademique != null && !this.idAnneeAcademique.equals(other.idAnneeAcademique))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.AnneeAcademique[ idAnneeAcademique=" + idAnneeAcademique + " ]";
    }
    
}
