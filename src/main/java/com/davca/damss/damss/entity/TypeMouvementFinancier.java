/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "type_mouvement_financier")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeMouvementFinancier.findAll", query = "SELECT t FROM TypeMouvementFinancier t")
    , @NamedQuery(name = "TypeMouvementFinancier.findByIdtypeMouvementFinancier", query = "SELECT t FROM TypeMouvementFinancier t WHERE t.idtypeMouvementFinancier = :idtypeMouvementFinancier")
    , @NamedQuery(name = "TypeMouvementFinancier.findByIntitule", query = "SELECT t FROM TypeMouvementFinancier t WHERE t.intitule = :intitule")
    , @NamedQuery(name = "TypeMouvementFinancier.findByDescription", query = "SELECT t FROM TypeMouvementFinancier t WHERE t.description = :description")})
public class TypeMouvementFinancier implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtype_mouvement_financier")
    private Long idtypeMouvementFinancier;
    @Size(max = 45)
    @Column(name = "intitule")
    private String intitule;
    @Size(max = 45)
    @Column(name = "description")
    private String description;
    @Size(max = 45)
    @Column(name = "statut")
    private String statut;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "typeMouvementFinancierIdtypeMouvementFinancier")
    private List<MouvementFinancier> mouvementFinancierList;

    public TypeMouvementFinancier() {
    }

    public TypeMouvementFinancier(Long idtypeMouvementFinancier) {
        this.idtypeMouvementFinancier = idtypeMouvementFinancier;
    }

    public Long getIdtypeMouvementFinancier() {
        return idtypeMouvementFinancier;
    }

    public void setIdtypeMouvementFinancier(Long idtypeMouvementFinancier) {
        this.idtypeMouvementFinancier = idtypeMouvementFinancier;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @XmlTransient
    public List<MouvementFinancier> getMouvementFinancierList() {
        return mouvementFinancierList;
    }

    public void setMouvementFinancierList(List<MouvementFinancier> mouvementFinancierList) {
        this.mouvementFinancierList = mouvementFinancierList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtypeMouvementFinancier != null ? idtypeMouvementFinancier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeMouvementFinancier)) {
            return false;
        }
        TypeMouvementFinancier other = (TypeMouvementFinancier) object;
        if ((this.idtypeMouvementFinancier == null && other.idtypeMouvementFinancier != null) || (this.idtypeMouvementFinancier != null && !this.idtypeMouvementFinancier.equals(other.idtypeMouvementFinancier))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.TypeMouvementFinancier[ idtypeMouvementFinancier=" + idtypeMouvementFinancier + " ]";
    }
}
