package com.davca.damss.damss.entity;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.davca.damss.damss.entity.Inscription;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "classe")
@XmlRootElement

public class Classe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idclasse")
    private Long idclasse;
    @Size(max = 45)
    @Column(name = "intitule")
    private String intitule;
    @Size(max = 15)
    @Column(name = "cycle")
    private String cycle;
    @Size(max = 10)
    @Column(name = "niveau")
    private String niveau;
    @Column(name = "effectifs")
    private Integer effectifs;
    @Size(max = 50)
    @Column(name = "description")
    private String description;
    @Size(max = 45)
    @Column(name = "classecol")
    private String classecol;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classeIdclasse")
    private List<ModaliteClasse> modaliteClasseList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "classeIdclasse")
    private List<Inscription> inscriptionList;

    public Classe() {
    }

    public Classe(Long idclasse) {
        this.idclasse = idclasse;
    }

    public Long getIdclasse() {
        return idclasse;
    }

    public void setIdclasse(Long idclasse) {
        this.idclasse = idclasse;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getCycle() {
        return cycle;
    }

    public void setCycle(String cycle) {
        this.cycle = cycle;
    }

    public String getNiveau() {
        return niveau;
    }

    public void setNiveau(String niveau) {
        this.niveau = niveau;
    }

    public Integer getEffectifs() {
        return effectifs;
    }

    public void setEffectifs(Integer effectifs) {
        this.effectifs = effectifs;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getClassecol() {
        return classecol;
    }

    public void setClassecol(String classecol) {
        this.classecol = classecol;
    }

    @XmlTransient
    public List<ModaliteClasse> getModaliteClasseList() {
        return modaliteClasseList;
    }

    public void setModaliteClasseList(List<ModaliteClasse> modaliteClasseList) {
        this.modaliteClasseList = modaliteClasseList;
    }

    @XmlTransient
    public List<Inscription> getInscriptionList() {
        return inscriptionList;
    }

    public void setInscriptionList(List<Inscription> inscriptionList) {
        this.inscriptionList = inscriptionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idclasse != null ? idclasse.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Classe)) {
            return false;
        }
        Classe other = (Classe) object;
        if ((this.idclasse == null && other.idclasse != null) || (this.idclasse != null && !this.idclasse.equals(other.idclasse))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.Classe[ idclasse=" + idclasse + " ]";
    }
    
}
