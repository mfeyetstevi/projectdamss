/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "paiement")
@XmlRootElement

public class Paiement implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idpaiement")
    private Integer idpaiement;
    @Size(max = 45)
    @Column(name = "paiementcol")
    private String paiementcol;
    @Column(name = "type_operation")
    private Boolean typeOperation;
    @Size(max = 30)
    @Column(name = "mode_reglement")
    private String modeReglement;
    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "montant")
    private Float montant;
    @Size(max = 50)
    @Column(name = "libelle")
    private String libelle;
    @JoinColumn(name = "eleve_ideleve", referencedColumnName = "idELEVE")
    @ManyToOne(optional = false)
    private Eleve eleveIdeleve;
    @JoinColumn(name = "mouvement_financier_idmouvement_financier", referencedColumnName = "idMouvement_financier")
    @ManyToOne(optional = false)
    private MouvementFinancier mouvementFinancierIdmouvementFinancier;
    @JoinColumn(name = "compte_tresorerie_idcompte-tresorerie", referencedColumnName = "idcompte_tresorerie")
    @ManyToOne(optional = false)
    private CompteTresorerie compteTresorerieIdcompteTresorerie;
    @JoinColumn(name = "utilisateur_idutilisateur", referencedColumnName = "idutilisateur")
    @ManyToOne(optional = false)
    private Utilisateur utilisateurIdutilisateur;

    public Paiement() {
    }

    public Paiement(Integer idpaiement) {
        this.idpaiement = idpaiement;
    }

    public Integer getIdpaiement() {
        return idpaiement;
    }

    public void setIdpaiement(Integer idpaiement) {
        this.idpaiement = idpaiement;
    }

    public String getPaiementcol() {
        return paiementcol;
    }

    public void setPaiementcol(String paiementcol) {
        this.paiementcol = paiementcol;
    }

    public Boolean getTypeOperation() {
        return typeOperation;
    }

    public void setTypeOperation(Boolean typeOperation) {
        this.typeOperation = typeOperation;
    }

    public String getModeReglement() {
        return modeReglement;
    }

    public void setModeReglement(String modeReglement) {
        this.modeReglement = modeReglement;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Float getMontant() {
        return montant;
    }

    public void setMontant(Float montant) {
        this.montant = montant;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Eleve getEleveIdeleve() {
        return eleveIdeleve;
    }

    public void setEleveIdeleve(Eleve eleveIdeleve) {
        this.eleveIdeleve = eleveIdeleve;
    }

    public MouvementFinancier getMouvementFinancierIdmouvementFinancier() {
        return mouvementFinancierIdmouvementFinancier;
    }

    public void setMouvementFinancierIdmouvementFinancier(MouvementFinancier mouvementFinancierIdmouvementFinancier) {
        this.mouvementFinancierIdmouvementFinancier = mouvementFinancierIdmouvementFinancier;
    }

    public CompteTresorerie getCompteTresorerieIdcompteTresorerie() {
        return compteTresorerieIdcompteTresorerie;
    }

    public void setCompteTresorerieIdcompteTresorerie(CompteTresorerie compteTresorerieIdcompteTresorerie) {
        this.compteTresorerieIdcompteTresorerie = compteTresorerieIdcompteTresorerie;
    }

    public Utilisateur getUtilisateurIdutilisateur() {
        return utilisateurIdutilisateur;
    }

    public void setUtilisateurIdutilisateur(Utilisateur utilisateurIdutilisateur) {
        this.utilisateurIdutilisateur = utilisateurIdutilisateur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idpaiement != null ? idpaiement.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paiement)) {
            return false;
        }
        Paiement other = (Paiement) object;
        if ((this.idpaiement == null && other.idpaiement != null) || (this.idpaiement != null && !this.idpaiement.equals(other.idpaiement))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.Paiement[ idpaiement=" + idpaiement + " ]";
    }
    
}

