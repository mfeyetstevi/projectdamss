/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import static javax.swing.text.StyleConstants.Size;
import javax.validation.constraints.Size;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author isnov-pc
 */
@Entity
public class Employe implements Serializable {

   private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idemploye")
    private Long idemploye;
    @Size(max = 45)
    @Column(name = "nom")
    private String nom;
    
     @Size(max = 45)
    @Column(name = "matricule")
    private String matricule;
    
    
    @Size(max = 45)
    @Column(name = "prenom")
    private String prenom;
    
   @Temporal(TemporalType.DATE)
    @Column(name = "datedebutservice")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date datedebutservice;
    
    @Size(max = 45)
    @Column(name = "ville")
    private String ville;
    
    @Size(max = 45)
    @Column(name = "age")
    private String age;
    
    @Column(name = "numero_tel")
    private Integer numeroTel;
    @Size(max = 30)
    @Column(name = "nationalite")
    private String nationalite;
    @Column(name = "date_naissance")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date dateNaissance;
    @Size(max = 45)
    @Column(name = "lieu_naissance")
    private String lieuNaissance;
    @Size(max = 45)
    @Column(name = "situation_matrimoniale")
    private String situationMatrimoniale;
    @Size(max = 30)
    @Column(name = "nom_quartier_residence")
    private String nomQuartierResidence;
    @Column(name = "numero_CNI")
    private Integer numeroCNI;
    @Column(name = "date_delivrance_CNI")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date datedelivranceCNI;
    @Column(name = "date_expirattioon_CNI")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date dateexpirattioonCNI;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "e_mail")
    private String eMail;
    @Size(max = 45)
    @Column(name = "fonction")
    private String fonction;
    
    @Size(max = 45)
    @Column(name = "adresse")
    private String adresse;
    
    
    
    @Size(max = 45)
    @Column(name = "echelon")
    private String echelon;
    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date dateCreation;
    @Column(name = "vacataire")
    private String vacataire;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "nombre_annee_experience")
    private Double nombreAnneeExperience;
    @Column(name = "sexe")
    private String sexe;

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public Date getDatedebutservice() {
        return datedebutservice;
    }

    public void setDatedebutservice(Date datedebutservice) {
        this.datedebutservice = datedebutservice;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    
    
    
    
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
    

    public Employe() {
    }

    public Employe(Long idemploye) {
        this.idemploye = idemploye;
    }

 public Long getIdemploye() {
        return idemploye;
    }

    public void setIdemploye(Long idemploye) {
        this.idemploye = idemploye;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getNumeroTel() {
        return numeroTel;
    }

    public void setNumeroTel(Integer numeroTel) {
        this.numeroTel = numeroTel;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(Date dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getLieuNaissance() {
        return lieuNaissance;
    }

    public void setLieuNaissance(String lieuNaissance) {
        this.lieuNaissance = lieuNaissance;
    }

    public String getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(String situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public String getNomQuartierResidence() {
        return nomQuartierResidence;
    }

    public void setNomQuartierResidence(String nomQuartierResidence) {
        this.nomQuartierResidence = nomQuartierResidence;
    }

    public Integer getNumeroCNI() {
        return numeroCNI;
    }

    public void setNumeroCNI(Integer numeroCNI) {
        this.numeroCNI = numeroCNI;
    }

    public Date getDatedelivranceCNI() {
        return datedelivranceCNI;
    }

    public void setDatedelivranceCNI(Date datedelivranceCNI) {
        this.datedelivranceCNI = datedelivranceCNI;
    }

    public Date getDateexpirattioonCNI() {
        return dateexpirattioonCNI;
    }

    public void setDateexpirattioonCNI(Date dateexpirattioonCNI) {
        this.dateexpirattioonCNI = dateexpirattioonCNI;
    }

    public String getEMail() {
        return eMail;
    }

    public void setEMail(String eMail) {
        this.eMail = eMail;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }

    public String getEchelon() {
        return echelon;
    }

    public void setEchelon(String echelon) {
        this.echelon = echelon;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getVacataire() {
        return vacataire;
    }

    public void setVacataire(String vacataire) {
        this.vacataire = vacataire;
    }

    public Double getNombreAnneeExperience() {
        return nombreAnneeExperience;
    }

    public void setNombreAnneeExperience(Double nombreAnneeExperience) {
        this.nombreAnneeExperience = nombreAnneeExperience;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String geteMail() {
        return eMail;
    }

    public void seteMail(String eMail) {
        this.eMail = eMail;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idemploye != null ? idemploye.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employe)) {
            return false;
        }
        Employe other = (Employe) object;
        if ((this.idemploye == null && other.idemploye != null) || (this.idemploye != null && !this.idemploye.equals(other.idemploye))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Employe[ idemploye=" + idemploye + " ]";
    }

    
}
