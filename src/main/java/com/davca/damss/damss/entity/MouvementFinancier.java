/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author isnov-pc
 */
@Entity
@Table(name = "mouvement_financier")
@XmlRootElement

public class MouvementFinancier implements Serializable {

   private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idMouvement_financier")
    private Long idMouvementfinancier;
    @Size(max = 30)
    @Column(name = "code_mouvement_financier")
    private String codeMouvementFinancier;
    @Size(max = 45)
    @Column(name = "intitule")
    private String intitule;
    @Size(max = 45)
    @Column(name = "description")
    private String description;
    @Column(name = "Numcompte_general")
    private Integer numcomptegeneral;
    @Size(max = 30)
    @Column(name = "categorie")
    private String categorie;
    @Size(max = 45)
    @Column(name = "statut")
    private String statut;
    @JoinColumn(name = "type_mouvement_financier_idtype_mouvement_financier", referencedColumnName = "idtype_mouvement_financier")
    @ManyToOne(optional = false)
    private TypeMouvementFinancier typeMouvementFinancierIdtypeMouvementFinancier;

    public MouvementFinancier() {
    }

    public MouvementFinancier(Long idMouvementfinancier) {
        this.idMouvementfinancier = idMouvementfinancier;
    }

    public Long getIdMouvementfinancier() {
        return idMouvementfinancier;
    }

    public void setIdMouvementfinancier(Long idMouvementfinancier) {
        this.idMouvementfinancier = idMouvementfinancier;
    }

    public String getCodeMouvementFinancier() {
        return codeMouvementFinancier;
    }

    public void setCodeMouvementFinancier(String codeMouvementFinancier) {
        this.codeMouvementFinancier = codeMouvementFinancier;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(String intitule) {
        this.intitule = intitule;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumcomptegeneral() {
        return numcomptegeneral;
    }

    public void setNumcomptegeneral(Integer numcomptegeneral) {
        this.numcomptegeneral = numcomptegeneral;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public TypeMouvementFinancier getTypeMouvementFinancierIdtypeMouvementFinancier() {
        return typeMouvementFinancierIdtypeMouvementFinancier;
    }

    public void setTypeMouvementFinancierIdtypeMouvementFinancier(TypeMouvementFinancier typeMouvementFinancierIdtypeMouvementFinancier) {
        this.typeMouvementFinancierIdtypeMouvementFinancier = typeMouvementFinancierIdtypeMouvementFinancier;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMouvementfinancier != null ? idMouvementfinancier.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MouvementFinancier)) {
            return false;
        }
        MouvementFinancier other = (MouvementFinancier) object;
        if ((this.idMouvementfinancier == null && other.idMouvementfinancier != null) || (this.idMouvementfinancier != null && !this.idMouvementfinancier.equals(other.idMouvementfinancier))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.davca.springdemo.entity.MouvementFinancier[ idMouvementfinancier=" + idMouvementfinancier + " ]";
    }
}
