/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;

/**
 *
 * @author isnov-pc
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
     @Autowired
 public void globalConfig(AuthenticationManagerBuilder auth, DataSource dataSource) throws Exception{
     
   auth.inMemoryAuthentication().withUser("admin").password("123").roles("ADMIN","PROF");
         auth.inMemoryAuthentication().withUser("prof1").password("123").roles("PROF");
             auth.inMemoryAuthentication().withUser("et1").password("123").roles("ETUDIANT");
                 auth.inMemoryAuthentication().withUser("sco1").password("123").roles("SCOLARITE");
 }
  
 @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                
                .authorizeRequests() 
                .antMatchers("/css/**","/js/**","/images/**","/assets/**").permitAll()
                .anyRequest()
                .authenticated()
                .and()
                 
            .formLogin()
                
                .loginPage("/login")
                .permitAll()
                .defaultSuccessUrl("/home")
                
                .failureUrl("/error.html")
                ;
    }
    
    @SuppressWarnings("deprecation")
@Bean
public static NoOpPasswordEncoder passwordEncoder() {
return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
}
 
 
 
 
}
