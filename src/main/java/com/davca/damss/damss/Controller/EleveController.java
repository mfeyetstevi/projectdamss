/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.EleveRestService;
import com.davca.damss.damss.entity.Eleve;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class EleveController {
    
    private final EleveRestService eleveservice;

    @Autowired
    public EleveController(EleveRestService eleveservice) {
        this.eleveservice = eleveservice;
    }

    @RequestMapping(value = "/elevelist", method = RequestMethod.GET)
    public String homeListPage(Model model) {
        model.addAttribute("eleves", eleveservice.getAvailbleEleve());
        return "listStudents";
    }



    @RequestMapping(value = "/addeleve", method = RequestMethod.POST)
    public String addPageEleve(@ModelAttribute Eleve person, Model model) {
  
        eleveservice.createEleve(person);
        model.addAttribute("eleves", eleveservice.getAllEleve());
        return "redirect:/elevelist";
    }



    @RequestMapping(value = "addeleve")
    public String addEleve(Model model) {
        model.addAttribute("eleve", new Eleve());
        return "sglStudent";
    }

    @RequestMapping(value = "/editeleve", method = RequestMethod.POST)
    public String editEleve(@ModelAttribute Eleve person, Model model) {
        
        eleveservice.editEleve(person);
        model.addAttribute("eleves", eleveservice.getAllEleve());
        return "redirect:/elevelist";
    }

    @RequestMapping(value = "/editeleve/{id}")
    public String editEleve(@PathVariable("id") Long eleveId, Model model) {
        model.addAttribute("eleve", eleveservice.getEleve(eleveId));
        return "sglEditStudent";
    }
    @RequestMapping(value = "/vieweleve/{id}")
    public String viewEleve(@PathVariable("id") Long eleveId, Model model) {
        model.addAttribute("eleve", eleveservice.getEleve(eleveId));
        return "viewStudent";
    }

    @RequestMapping(value = "/deleteeleve/{id}", method = RequestMethod.GET)
    public String deleteEleve(@PathVariable("id") Long eleveId, Model model) {
        eleveservice.getEleve(eleveId).setStatut("Inactif");
        eleveservice.createEleve( eleveservice.getEleve(eleveId));
        return "redirect:/elevelist";
    }
}
