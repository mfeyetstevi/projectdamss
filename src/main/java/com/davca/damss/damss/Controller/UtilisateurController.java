/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;


import com.davca.damss.damss.Service.EmployeRestService;
import com.davca.damss.damss.Service.ProfilRestService;
import com.davca.damss.damss.Service.UtilisateurRestService;
import com.davca.damss.damss.entity.ProfilUtilisateur;
import com.davca.damss.damss.entity.Utilisateur;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class UtilisateurController {
  
    
    public final UtilisateurRestService utilisateurRestService;
    
    public final EmployeRestService employeservice;
    
    public final ProfilRestService profilservice;
    
    public UtilisateurController(UtilisateurRestService p,EmployeRestService s,ProfilRestService r){
      
        this.utilisateurRestService=p; 
        this.employeservice=s;
        this.profilservice=r;
    }
    
  @RequestMapping(value = "/userlist", method = RequestMethod.GET)
    public String homeListPage(Model model) {

        model.addAttribute("users", utilisateurRestService.getAllUtilisateur());
        return "listUser";
    }



    @RequestMapping(value = "/adduser", method = RequestMethod.POST)
    public String addPageUser(@ModelAttribute ProfilUtilisateur person, Model model) {
        
        utilisateurRestService.createUtilisateur(person);
        model.addAttribute("users", utilisateurRestService.getAllUtilisateur());
        return "redirect:/userlist";
    }



    @RequestMapping(value = "adduser")
    public String addUser(Model model) {
        model.addAttribute("user", new ProfilUtilisateur());
        model.addAttribute("employes", employeservice.getAllEmploye());
         model.addAttribute("profils", profilservice.getAllProfil());
        return "sglUser";
    }

    @RequestMapping(value = "/edituser", method = RequestMethod.POST)
    public String editUser(@ModelAttribute Utilisateur person, Model model) {
     
        model.addAttribute("users", utilisateurRestService.getAllUtilisateur());
        return "redirect:/userlist";
    }

    @RequestMapping(value = "/edituser/{id}")
    public String editEUser(@PathVariable("id") Long profilId, Model model) {
        model.addAttribute("user", utilisateurRestService.getUtilisateur(profilId));
        model.addAttribute("employes", employeservice.getAllEmploye());
        model.addAttribute("profils", profilservice.getAllProfil());
        return "sglEditUser";
    }
    @RequestMapping(value = "/viewuser/{id}")
    public String viewUser(@PathVariable("id") Long profilId, Model model) {
        model.addAttribute("user", utilisateurRestService.getUtilisateur(profilId));
        model.addAttribute("employes", employeservice.getAllEmploye());
        model.addAttribute("profils", profilservice.getAllProfil());
        return "viewUser";
    }

    @RequestMapping(value = "/deleteuser/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("id") Long profilId, Model model) {
        utilisateurRestService.deleteUtilisateur(profilId);
        return "redirect:/userlist";
    }

    
    
    
    
    
    
    
    
}
