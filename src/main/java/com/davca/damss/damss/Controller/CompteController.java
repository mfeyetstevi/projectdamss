/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.CompteRestService;
import com.davca.damss.damss.entity.CompteTresorerie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class CompteController {
    
    public final CompteRestService compteResyService;
    
    @Autowired
    public CompteController(CompteRestService compteResyService){
        
        this.compteResyService=compteResyService;
        
    }
    
    
    @RequestMapping(value = "/comptelist", method = RequestMethod.GET)
    public String homeListPage(Model model) {
        model.addAttribute("comptes", compteResyService.getAvailbleCompteTresorerie());
        return "listCompteTresorerie";
    }



    @RequestMapping(value = "/addcompte", method = RequestMethod.POST)
    public String addPageCompteTresorerie(@ModelAttribute CompteTresorerie person, Model model) {
  
        compteResyService.createCompteTresorerie(person);
        model.addAttribute("comptes", compteResyService.getAllCompteTresorerie());
        return "redirect:/comptelist";
    }



    @RequestMapping(value = "addcompte")
    public String addCompteTresorerie(Model model) {
        model.addAttribute("compte", new CompteTresorerie());
        return "sglCompteTresorerie";
    }

    @RequestMapping(value = "/editcompte", method = RequestMethod.POST)
    public String editCompteTresorerie(@ModelAttribute CompteTresorerie person, Model model) {
        
        compteResyService.editCompteTresorerie(person);
        model.addAttribute("comptes", compteResyService.getAllCompteTresorerie());
        return "redirect:/comptelist";
    }

    @RequestMapping(value = "/editcompte/{id}")
    public String editCompteTresorerie(@PathVariable("id") Long compteId, Model model) {
        model.addAttribute("compte", compteResyService.getCompteTresorerie(compteId));
        return "sglEditCompteTresorerie";
    }
    @RequestMapping(value = "/viewcompte/{id}")
    public String viewCompteTresorerie(@PathVariable("id") Long compteId, Model model) {
        model.addAttribute("compte", compteResyService.getCompteTresorerie(compteId));
        return "viewCompteTresorerie";
    }

    @RequestMapping(value = "/deletecompte/{id}", method = RequestMethod.GET)
    public String deleteCompteTresorerie(@PathVariable("id") Long compteId, Model model) {
        compteResyService.deleteCompteTresorerie(compteId);
      
        return "redirect:/comptelist";
    }
    
}
