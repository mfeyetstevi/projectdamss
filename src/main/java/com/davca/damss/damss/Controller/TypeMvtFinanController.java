/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.TypeMvtFinanRestService;
import com.davca.damss.damss.entity.TypeMouvementFinancier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class TypeMvtFinanController {
    
    public final TypeMvtFinanRestService typeMvtRestService;
    
    @Autowired
    public TypeMvtFinanController(TypeMvtFinanRestService typeMvtRestService){
        this.typeMvtRestService=typeMvtRestService;
        
    }
    
    
    @RequestMapping(value = "/typemvtlist", method = RequestMethod.GET)
    public String homeListPage(Model model) {
        model.addAttribute("typemvts", typeMvtRestService.getAvailbleTypeMouvementFinancier());
        return "listTypeMvtFin";
    }



    @RequestMapping(value = "/addtypemvt", method = RequestMethod.POST)
    public String addPageTypeMvtFin(@ModelAttribute TypeMouvementFinancier person, Model model) {
  
        typeMvtRestService.createTypeMouvementFinancier(person);
        
        return "redirect:/typemvtlist";
    }



    @RequestMapping(value = "addtypemvt")
    public String addTypeMvtFin(Model model) {
        model.addAttribute("typemvt", new TypeMouvementFinancier());
        return "sglTypeMvtFin";
    }

    @RequestMapping(value = "/edittypemvt", method = RequestMethod.POST)
    public String editTypeMvtFin(@ModelAttribute TypeMouvementFinancier person, Model model) {
        
        typeMvtRestService.editTypeMouvementFinancier(person);
      
        return "redirect:/typemvtlist";
    }

    @RequestMapping(value = "/edittypemvt/{id}")
    public String editTypeMvtFin(@PathVariable("id") Long typemvtId, Model model) {
        model.addAttribute("typemvt", typeMvtRestService.getTypeMouvementFinancier(typemvtId));
        return "sglEditTypeMvtFin";
    }
    @RequestMapping(value = "/viewtypemvt/{id}")
    public String viewTypeMvtFin(@PathVariable("id") Long typemvtId, Model model) {
        model.addAttribute("typemvt",typeMvtRestService.getTypeMouvementFinancier(typemvtId) );
        return "viewTypeMvtFin";
    }

    @RequestMapping(value = "/deletetypemvt/{id}", method = RequestMethod.GET)
    public String deleteTypeMvtFin(@PathVariable("id") Long typemvtId, Model model) {
        typeMvtRestService.deleteTypeMouvementFinancier(typemvtId);
      
        return "redirect:/typemvtlist";
    }
    
    
}
