/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.MvtFinancierRestService;
import com.davca.damss.damss.Service.TypeMvtFinanRestService;
import com.davca.damss.damss.entity.MouvementFinancier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class MvtFinancierController {
    
    public final MvtFinancierRestService mvtFinanRestService;
    public final TypeMvtFinanRestService typMvtFinanser;
    
    @Autowired
    public MvtFinancierController(MvtFinancierRestService mvtFinanRestService ,TypeMvtFinanRestService typMvtFinanser){
        
        this.mvtFinanRestService=mvtFinanRestService;this.typMvtFinanser=typMvtFinanser;
    }
    
 @RequestMapping(value = "/mvtlist", method = RequestMethod.GET)
    public String homeListPage(Model model) {
        model.addAttribute("mvts", mvtFinanRestService.getAllMouvementFinancier());
        
        return "listMvtFin";
    }



    @RequestMapping(value = "/addmvt", method = RequestMethod.POST)
    public String addPageTypeMvtFin(@ModelAttribute MouvementFinancier person, Model model) {
  
        mvtFinanRestService.createMouvementFinancier(person);
        
        return "redirect:/mvtlist";
    }



    @RequestMapping(value = "addmvt")
    public String addTypeMvtFin(Model model) {
        model.addAttribute("mvt", new MouvementFinancier());
        model.addAttribute("typemvts", typMvtFinanser.getAvailbleTypeMouvementFinancier());
        return "sglMvtFin";
    }

    @RequestMapping(value = "/editmvt", method = RequestMethod.POST)
    public String editTypeMvtFin(@ModelAttribute MouvementFinancier person, Model model) {
        
        mvtFinanRestService.editMouvementFinancier(person);
      
        return "redirect:/mvtlist";
    }

    @RequestMapping(value = "/editmvt/{id}")
    public String editTypeMvtFin(@PathVariable("id") Long mvtId, Model model) {
        model.addAttribute("mvt", mvtFinanRestService.getMouvementFinancier(mvtId));
        return "sglEditMvtFin";
    }
    @RequestMapping(value = "/viewmvt/{id}")
    public String viewTypeMvtFin(@PathVariable("id") Long mvtId, Model model) {
        model.addAttribute("mvt",mvtFinanRestService.getMouvementFinancier(mvtId) );
        return "viewMvtFin";
    }

    @RequestMapping(value = "/deletemvt/{id}", method = RequestMethod.GET)
    public String deleteTypeMvtFin(@PathVariable("id") Long mvtId, Model model) {
        mvtFinanRestService.deleteMouvementFinancier(mvtId);
      
        return "redirect:/mvtlist";
    }
       
    
    
}
