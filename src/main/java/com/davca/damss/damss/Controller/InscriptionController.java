/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.ClasseRestService;
import com.davca.damss.damss.Service.EleveRestService;
import com.davca.damss.damss.Service.InscriptionRestService;
import com.davca.damss.damss.Service.UtilisateurRestService;
import com.davca.damss.damss.entity.Inscription;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class InscriptionController {
    private final InscriptionRestService inscriptionservice;
    private final EleveRestService eleveRestService;
    private final ClasseRestService classeRestService;
    private final UtilisateurRestService utilisateurRestservice;

    @Autowired
    public InscriptionController(InscriptionRestService inscriptionservice,EleveRestService eleveRestService,ClasseRestService classeRestService,UtilisateurRestService utilisateurRestservice) {
        this.inscriptionservice = inscriptionservice;
        this.eleveRestService=eleveRestService;
        this.classeRestService=classeRestService;
        this.utilisateurRestservice=utilisateurRestservice;
    }

    @RequestMapping(value = "/inscriptionlist", method = RequestMethod.GET)
    public String homeListPage(Model model) {

        model.addAttribute("inscriptions", inscriptionservice.getAllInscription());
       
        return "listEntries";
    }

 

    @RequestMapping(value = "/addinscription", method = RequestMethod.POST)
    public String addPageInscription(@ModelAttribute Inscription person, Model model) {
 person.setUtilisateurIdutilisateur(utilisateurRestservice.getUtilisateurOne(new Long(2)));
        person.setDateInscription(new Date());
        inscriptionservice.createInscription(person);
        model.addAttribute("inscriptions", inscriptionservice.getAllInscription());
        return "redirect:/inscriptionlist";
    }



    @RequestMapping(value = "/addinscription")
    public String addInscription(Model model) {
        model.addAttribute("inscription", new Inscription());
        model.addAttribute("eleves", eleveRestService.getAvailbleEleve());
        model.addAttribute("classes",classeRestService.getAllClasse() );
        
        return "sglEntrie";
    }

    @RequestMapping(value = "/editinscrip", method = RequestMethod.POST)
    public String editInscription(@ModelAttribute Inscription person, Model model) {
        
        inscriptionservice.editInscription(person);
        model.addAttribute("eleves", eleveRestService.getAvailbleEleve());
        model.addAttribute("classes",classeRestService.getAllClasse() );
        model.addAttribute("inscriptions", inscriptionservice.getAllInscription());
        return "redirect:/inscriptionlist";
    }

    @RequestMapping(value = "/editinscrip/{id}")
    public String editInscription(@PathVariable("id") Long inscriptionId, Model model) {
        model.addAttribute("inscription", inscriptionservice.getInscription(inscriptionId));
        return "sglEditEntrie";
    }
    @RequestMapping(value = "/viewinscrip/{id}")
    public String viewInscription(@PathVariable("id") Long inscriptionId, Model model) {
        model.addAttribute("inscription", inscriptionservice.getInscription(inscriptionId));
        model.addAttribute("eleves", eleveRestService.getAvailbleEleve());
        model.addAttribute("classes",classeRestService.getAllClasse() );
        return "viewEntrie";
    }

    @RequestMapping(value = "/deleteinsri/{id}", method = RequestMethod.GET)
    public String deleteInscription(@PathVariable("id") Long inscriptionId, Model model) {
        inscriptionservice.deleteInscription(inscriptionId);
        return "redirect:/inscriptionlist";
    }
}
