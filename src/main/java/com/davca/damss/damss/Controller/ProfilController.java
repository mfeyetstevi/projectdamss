/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.ProfilRestService;
import com.davca.damss.damss.entity.Profil;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class ProfilController {
    
    
    public final ProfilRestService profilRestService; 
    
    @Autowired
    public ProfilController(ProfilRestService p){
        
        this.profilRestService=p;
    }
    
    @RequestMapping(value = "/profillist", method = RequestMethod.GET)
    public String homeListPage(Model model) {

        model.addAttribute("profils", profilRestService.getAllProfil());
        return "listRole";
    }



    @RequestMapping(value = "/addprofil", method = RequestMethod.POST)
    public String addPageProfil(@ModelAttribute Profil person, Model model) {
        
        profilRestService.createProfil(person);
        model.addAttribute("profils", profilRestService.getAllProfil());
        return "redirect:/profillist";
    }



    @RequestMapping(value = "addprofil")
    public String addProfil(Model model) {
        model.addAttribute("profil", new Profil());
        return "sglRole";
    }

    @RequestMapping(value = "/editprofil", method = RequestMethod.POST)
    public String editProfil(@ModelAttribute Profil person, Model model) {
        
        profilRestService.createProfil(person);
        model.addAttribute("profils", profilRestService.getAllProfil());
        return "redirect:/profillist";
    }

    @RequestMapping(value = "/editprofil/{id}")
    public String editEProfil(@PathVariable("id") Long profilId, Model model) {
        model.addAttribute("profil", profilRestService.getProfil(profilId));
        return "sglEditRole";
    }
    @RequestMapping(value = "/viewprofil/{id}")
    public String viewProfil(@PathVariable("id") Long profilId, Model model) {
        model.addAttribute("profil", profilRestService.getProfil(profilId));
        return "viewRole";
    }

    @RequestMapping(value = "/deleteprofil/{id}", method = RequestMethod.GET)
    public String deleteProfil(@PathVariable("id") Long profilId, Model model) {
        profilRestService.deleteProfil(profilId);
        return "redirect:/profillist";
    }

    
}
