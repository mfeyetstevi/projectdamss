/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Controller;

import com.davca.damss.damss.Service.EmployeRestService;
import com.davca.damss.damss.entity.Employe;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author isnov-pc
 */
@Controller
public class EmployeController {

    private final EmployeRestService employeservice;

    @Autowired
    public EmployeController(EmployeRestService employeservice) {
        this.employeservice = employeservice;
    }

    @RequestMapping(value = "/employelist", method = RequestMethod.GET)
    public String homeListPage(Model model) {

        model.addAttribute("employes", employeservice.getAllEmploye());
        return "listEmploye";
    }

    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String homePage(Model model) {

        model.addAttribute("employes", employeservice.getAllEmploye());
        return "accueil";
    }

    @RequestMapping(value = "/addemploye", method = RequestMethod.POST)
    public String addPageEmploye(@ModelAttribute Employe person, Model model) {
        person.setDateCreation(new Date());
        employeservice.createEmploye(person);
        model.addAttribute("employes", employeservice.getAllEmploye());
        return "redirect:/employelist";
    }



    @RequestMapping(value = "addemploye")
    public String addEmploye(Model model) {
        model.addAttribute("employe", new Employe());
        return "sglEmployee";
    }

    @RequestMapping(value = "/editemp", method = RequestMethod.POST)
    public String editEmploye(@ModelAttribute Employe person, Model model) {
        
        employeservice.editEmploye(person);
        model.addAttribute("employes", employeservice.getAllEmploye());
        return "redirect:/employelist";
    }

    @RequestMapping(value = "/editemp/{id}")
    public String editEmploye(@PathVariable("id") Long employeId, Model model) {
        model.addAttribute("employe", employeservice.getEmploye(employeId));
        return "sglEditEmployee";
    }
    @RequestMapping(value = "/viewemp/{id}")
    public String viewEmploye(@PathVariable("id") Long employeId, Model model) {
        model.addAttribute("employe", employeservice.getEmploye(employeId));
        return "viewEmployee";
    }

    @RequestMapping(value = "/deleteemp/{id}", method = RequestMethod.GET)
    public String deleteEmploye(@PathVariable("id") Long employeId, Model model) {
        employeservice.deleteEmploye(employeId);
        return "redirect:/employelist";
    }

}
