/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.repository;

import com.davca.damss.damss.entity.Inscription;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author isnov-pc
 */
public interface InscriptionRepository extends JpaRepository<Inscription, Long> {
    
}
