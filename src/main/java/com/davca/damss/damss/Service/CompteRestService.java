/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.CompteTresorerie;
import com.davca.damss.damss.repository.CompteRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author isnov-pc
 */
@Service
public class CompteRestService implements CompteService{

    @Autowired
    private CompteRepository comteRepository;
    
    @Override
    public CompteTresorerie createCompteTresorerie(CompteTresorerie compte) {
        return comteRepository.save(compte);
    }

    @Override
    public CompteTresorerie getCompteTresorerie(Long id) {
      return comteRepository.getOne(id);
    }

    @Override
    public CompteTresorerie editCompteTresorerie(CompteTresorerie compte) {
        return comteRepository.save(compte);
    }

    @Override
    public void deleteCompteTresorerie(CompteTresorerie compte) {
    comteRepository.delete(compte);
    }

    @Override
    public void deleteCompteTresorerie(Long id) {
        
       CompteTresorerie r=comteRepository.getOne(id);
       r.setStatut("Inactif");
       comteRepository.save(r);
    }

    @Override
    public List<CompteTresorerie> getAllCompteTresorerie(int pageNumber, int pageSiz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<CompteTresorerie> getAllCompteTresorerie() {
        return comteRepository.findAll();
    }
    public List<CompteTresorerie> getAvailbleCompteTresorerie() {
         List<CompteTresorerie> el= new ArrayList<CompteTresorerie>();
         for(CompteTresorerie e:comteRepository.findAll()){
             
             if(e.getStatut().equals("Actif") ){
                 el.add(e);
             }
         }
         
       return el;
    }
}
