/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Profil;
import com.davca.damss.damss.entity.ProfilUtilisateur;
import com.davca.damss.damss.entity.Utilisateur;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface UtilisateurService {
    
    ProfilUtilisateur createUtilisateur(ProfilUtilisateur profil);
ProfilUtilisateur getUtilisateur(Long id);
Utilisateur editUtilisateur(Utilisateur profil);
void deleteUtilisateur(Utilisateur profil);
void deleteUtilisateur(Long id);
List<Utilisateur> getAllUtilisateur(int pageNumber, int pageSiz);
List<Utilisateur> getAllUtilisateur();
void assignRoleToUser(Utilisateur user,Profil pro);

void deleteAllProfilUtilisateur(Utilisateur u);
}
