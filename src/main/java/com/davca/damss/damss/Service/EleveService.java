/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Eleve;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface EleveService {
     Eleve createEleve(Eleve employe);
Eleve getEleve(Long id);
Eleve editEleve(Eleve employe);
void deleteEleve(Eleve employe);
void deleteEleve(Long id);
List<Eleve> getAllEleve(int pageNumber, int pageSiz);
List<Eleve> getAllEleve();
}
