/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Classe;
import com.davca.damss.damss.repository.ClasseRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author isnov-pc
 */
@Service
public class ClasseRestService implements ClasseService{

    @Autowired
    private ClasseRepository classeRepository;
    
    @Override
    public Classe createClasse(Classe classe) {
     return classeRepository.save(classe);
    }

    @Override
    public Classe getClasse(Long id) {
        return classeRepository.getOne(id);
    }

    @Override
    public Classe editClasse(Classe classe) {
return classeRepository.save(classe);
    }

    @Override
    public void deleteClasse(Classe classe) {
   classeRepository.delete(classe);
    }

    @Override
    public void deleteClasse(Long id) {
       classeRepository.deleteById(id);
    }

    @Override
    public List<Classe> getAllClasse(int pageNumber, int pageSiz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Classe> getAllClasse() {
     return classeRepository.findAll();
    }
    
}
