/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.MouvementFinancier;
import com.davca.damss.damss.repository.MvtFinancierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author isnov-pc
 */
@Service
public class MvtFinancierRestService implements MvtFinancierService {

    @Autowired
    private MvtFinancierRepository mvtFinRep;
    
    @Override
    public MouvementFinancier createMouvementFinancier(MouvementFinancier inscri) {
      return mvtFinRep.save(inscri);
    }

    @Override
    public MouvementFinancier getMouvementFinancier(Long id) {
        return mvtFinRep.getOne(id);
    }

    @Override
    public MouvementFinancier editMouvementFinancier(MouvementFinancier inscri) {
       return mvtFinRep.save(inscri);
    }

    @Override
    public void deleteMouvementFinancier(MouvementFinancier inscri) {
      mvtFinRep.delete(inscri);
    }

    @Override
    public void deleteMouvementFinancier(Long id) {
        mvtFinRep.deleteById(id);
    }

    @Override
    public List<MouvementFinancier> getAllMouvementFinancier(int pageNumber, int pageSiz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<MouvementFinancier> getAllMouvementFinancier() {
        return mvtFinRep.findAll();
    }
        public List<MouvementFinancier> getAvailbleMouvementFinancier() {
         List<MouvementFinancier> el= new ArrayList<MouvementFinancier>();
         for(MouvementFinancier e:mvtFinRep.findAll()){
             
             if(e.getStatut().equals("Actif") ){
                 el.add(e);
             }
         }
         
       return el;
    }
}
