/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.MouvementFinancier;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface MvtFinancierService {
      MouvementFinancier createMouvementFinancier(MouvementFinancier inscri);
MouvementFinancier getMouvementFinancier(Long id);
MouvementFinancier editMouvementFinancier(MouvementFinancier inscri);
void deleteMouvementFinancier(MouvementFinancier inscri);
void deleteMouvementFinancier(Long id);
List<MouvementFinancier> getAllMouvementFinancier(int pageNumber, int pageSiz);
List<MouvementFinancier> getAllMouvementFinancier();   
}
