/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Inscription;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface InscriptionService {
    
     Inscription createInscription(Inscription inscri);
Inscription getInscription(Long id);
Inscription editInscription(Inscription inscri);
void deleteInscription(Inscription inscri);
void deleteInscription(Long id);
List<Inscription> getAllInscription(int pageNumber, int pageSiz);
List<Inscription> getAllInscription();
    
    
    
}
