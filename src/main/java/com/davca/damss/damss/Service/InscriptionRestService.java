/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Inscription;
import com.davca.damss.damss.repository.InscriptionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author isnov-pc
 */
@Service
public class InscriptionRestService implements InscriptionService {
    
    @Autowired
    private InscriptionRepository inscriptionRepository;

    @Override
    public Inscription createInscription(Inscription inscri) {
        return inscriptionRepository.save(inscri);
    }

    @Override
    public Inscription getInscription(Long id) {
    return inscriptionRepository.getOne(id);
    }

    @Override
    public Inscription editInscription(Inscription inscri) {
     return inscriptionRepository.save(inscri);
    }

    @Override
    public void deleteInscription(Inscription inscri) {
 inscriptionRepository.delete(inscri);
    }

    @Override
    public void deleteInscription(Long id) {
        inscriptionRepository.deleteById(id);
    }

    @Override
    public List<Inscription> getAllInscription(int pageNumber, int pageSiz) {
  return inscriptionRepository.findAll(new PageRequest(pageNumber, pageSiz)).getContent(); 
    }

    @Override
    public List<Inscription> getAllInscription() {
      return inscriptionRepository.findAll();
    }
    
}
