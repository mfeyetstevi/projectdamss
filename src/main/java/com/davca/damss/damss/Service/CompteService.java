/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.CompteTresorerie;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface CompteService {
     CompteTresorerie createCompteTresorerie(CompteTresorerie compte);
CompteTresorerie getCompteTresorerie(Long id);
CompteTresorerie editCompteTresorerie(CompteTresorerie compte);
void deleteCompteTresorerie(CompteTresorerie compte);
void deleteCompteTresorerie(Long id);
List<CompteTresorerie> getAllCompteTresorerie(int pageNumber, int pageSiz);
List<CompteTresorerie> getAllCompteTresorerie();
    
}
