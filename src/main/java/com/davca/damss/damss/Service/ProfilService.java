/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Profil;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface ProfilService {
    
    Profil createProfil(Profil profil);
Profil getProfil(Long id);
Profil editProfil(Profil profil);
void deleteProfil(Profil profil);
void deleteProfil(Long id);
List<Profil> getAllProfil(int pageNumber, int pageSiz);
List<Profil> getAllProfil();
}
