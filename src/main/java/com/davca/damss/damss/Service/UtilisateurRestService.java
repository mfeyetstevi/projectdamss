/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Profil;
import com.davca.damss.damss.entity.ProfilUtilisateur;
import com.davca.damss.damss.entity.Utilisateur;
import com.davca.damss.damss.repository.ProfilUtilisateurRepository;
import com.davca.damss.damss.repository.UtilisateurRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isnov-pc
 */
@Service
public class UtilisateurRestService implements UtilisateurService {
    
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    
    @Autowired
    private ProfilUtilisateurRepository profilUtilisateurRepository;
    
    
    
     

    @Override
    public ProfilUtilisateur createUtilisateur(ProfilUtilisateur profil) {
        
        utilisateurRepository.save(profil.getUtilisateurIdutilisateur());
   
        return profilUtilisateurRepository.save(profil);
    }

    
    @Override
    public ProfilUtilisateur getUtilisateur(Long id) {
      return  profilUtilisateurRepository.getOne(id);
    }
    
        public Utilisateur getUtilisateurOne(Long id) {
      return  utilisateurRepository.getOne(id);
    }

    @Override
    public Utilisateur editUtilisateur(Utilisateur profil) {
        return utilisateurRepository.save(profil);
    }

    @Override
    public void deleteUtilisateur(Utilisateur profil) {
        utilisateurRepository.delete(profil);
    }

    @Override
    public void deleteUtilisateur(Long id) {
       
         List<ProfilUtilisateur> pl=utilisateurRepository.getOne(id).getProfilUtilisateurList();
         
         for(ProfilUtilisateur n:pl){
             
             profilUtilisateurRepository.delete(n);
         }
         
         
        utilisateurRepository.deleteById(id);
    }
    
    

    @Override
    public List<Utilisateur> getAllUtilisateur(int pageNumber, int pageSiz) {
    return utilisateurRepository.findAll(new PageRequest(pageNumber, pageSiz)).getContent();
    }

    @Override
    public List<Utilisateur> getAllUtilisateur() {
    return  utilisateurRepository.findAll();
    }

    @Override
    public void assignRoleToUser(Utilisateur user, Profil pro) {
        
         ProfilUtilisateur pr= new ProfilUtilisateur();
       pr.setProfilIdprofil(pro);
       pr.setUtilisateurIdutilisateur(user);
       
       
       profilUtilisateurRepository.save(pr);
        
    }

    @Override
    public void deleteAllProfilUtilisateur(Utilisateur u) {

      List<ProfilUtilisateur> pl=u.getProfilUtilisateurList();
      
      
  
    }
    
    
    
}
