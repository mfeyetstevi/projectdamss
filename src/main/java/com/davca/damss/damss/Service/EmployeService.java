/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Employe;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface EmployeService {
    
    Employe createEmploye(Employe employe);
Employe getEmploye(Long id);
Employe editEmploye(Employe employe);
void deleteEmploye(Employe employe);
void deleteEmploye(Long id);
List<Employe> getAllEmploye(int pageNumber, int pageSiz);
List<Employe> getAllEmploye();
}
