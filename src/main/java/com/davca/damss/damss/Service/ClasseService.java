/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Classe;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface ClasseService {
    Classe createClasse(Classe classe);
Classe getClasse(Long id);
Classe editClasse(Classe classe);
void deleteClasse(Classe classe);
void deleteClasse(Long id);
List<Classe> getAllClasse(int pageNumber, int pageSiz);
List<Classe> getAllClasse();
}
