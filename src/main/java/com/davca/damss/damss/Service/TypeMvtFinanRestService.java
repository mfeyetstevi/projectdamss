/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.TypeMouvementFinancier;
import com.davca.damss.damss.repository.TypeMvtFinancierRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author isnov-pc
 */
@Service
public class TypeMvtFinanRestService implements TypeMvtFinancierService {

    @Autowired
    private TypeMvtFinancierRepository typeMvtRepo;
    
    
    @Override
    public TypeMouvementFinancier createTypeMouvementFinancier(TypeMouvementFinancier profil) {
       return typeMvtRepo.save(profil);
    }

    @Override
    public TypeMouvementFinancier getTypeMouvementFinancier(Long id) {
     return typeMvtRepo.getOne(id);
    }

    @Override
    public TypeMouvementFinancier editTypeMouvementFinancier(TypeMouvementFinancier profil) {
        return typeMvtRepo.save(profil);
    }

    @Override
    public void deleteTypeMouvementFinancier(TypeMouvementFinancier profil) {
        typeMvtRepo.delete(profil);
    }

    @Override
    public void deleteTypeMouvementFinancier(Long id) {
    
        TypeMouvementFinancier r=typeMvtRepo.getOne(id);
       r.setStatut("Inactif");
       typeMvtRepo.save(r);
        
        
    }

    @Override
    public List<TypeMouvementFinancier> getAllTypeMouvementFinancier(int pageNumber, int pageSiz) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<TypeMouvementFinancier> getAllTypeMouvementFinancier() {
      return typeMvtRepo.findAll();
    }
    public List<TypeMouvementFinancier> getAvailbleTypeMouvementFinancier() {
         List<TypeMouvementFinancier> el= new ArrayList<TypeMouvementFinancier>();
         for(TypeMouvementFinancier e:typeMvtRepo.findAll()){
             
             if(e.getStatut().equals("Actif") ){
                 el.add(e);
             }
         }
         
       return el;
    }
    
}
