/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Eleve;
import com.davca.damss.damss.repository.EleveRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author isnov-pc
 */
@Service
public class EleveRestService  implements EleveService{

    @Autowired
    private EleveRepository eleveRepository;
    
    
    
    @Override
    public Eleve createEleve(Eleve employe) {
       return eleveRepository.save(employe); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Eleve getEleve(Long id) {
      return   eleveRepository.getOne(id);
    }

    @Override
    public Eleve editEleve(Eleve employe) {
      return   eleveRepository.save(employe);
    }

    @Override
    public void deleteEleve(Eleve employe) {
       eleveRepository.delete(employe);
    }

    @Override
    public void deleteEleve(Long id) {
        eleveRepository.deleteById(id);
    }

    @Override
    public List<Eleve> getAllEleve(int pageNumber, int pageSiz) {
    
               return eleveRepository.findAll(new PageRequest(pageNumber, pageSiz)).getContent();
    }

    @Override
    public List<Eleve> getAllEleve() {
       return eleveRepository.findAll();
    }
    
    
     public List<Eleve> getAvailbleEleve() {
         List<Eleve> el= new ArrayList<Eleve>();
         for(Eleve e:eleveRepository.findAll()){
             
             if(e.getStatut().equals("Actif") ){
                 el.add(e);
             }
         }
         
       return el;
    }
    
}
