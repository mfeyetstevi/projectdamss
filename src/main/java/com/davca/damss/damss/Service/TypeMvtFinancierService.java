/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.TypeMouvementFinancier;
import java.util.List;

/**
 *
 * @author isnov-pc
 */
public interface TypeMvtFinancierService {
    TypeMouvementFinancier createTypeMouvementFinancier(TypeMouvementFinancier profil);
TypeMouvementFinancier getTypeMouvementFinancier(Long id);
TypeMouvementFinancier editTypeMouvementFinancier(TypeMouvementFinancier profil);
void deleteTypeMouvementFinancier(TypeMouvementFinancier profil);
void deleteTypeMouvementFinancier(Long id);
List<TypeMouvementFinancier> getAllTypeMouvementFinancier(int pageNumber, int pageSiz);
List<TypeMouvementFinancier> getAllTypeMouvementFinancier();
}
