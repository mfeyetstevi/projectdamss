/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;


import com.davca.damss.damss.entity.Profil;
import com.davca.damss.damss.repository.ProfilRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author isnov-pc
 */
@Service
public class ProfilRestService implements ProfilService {
    @Autowired
    private ProfilRepository profilRepository;
    
 

    @Override
    public Profil createProfil(Profil profil) {
   return profilRepository.save(profil);
    }

    @Override
    public Profil getProfil(Long id) {
        return profilRepository.getOne(id);
    }

    @Override
    public Profil editProfil(Profil profil) {
       return profilRepository.save(profil);
    }

    @Override
    public void deleteProfil(Profil profil) {
       
        profilRepository.delete(profil);
    }

    @Override
    public void deleteProfil(Long id) {
        profilRepository.deleteById(id);
    }

    @Override
    public List<Profil> getAllProfil(int pageNumber, int pageSiz) {
        return profilRepository.findAll(new PageRequest(pageNumber, pageSiz)).getContent();
    }

    @Override
    public List<Profil> getAllProfil() {
        return profilRepository.findAll();
    }
    
}
