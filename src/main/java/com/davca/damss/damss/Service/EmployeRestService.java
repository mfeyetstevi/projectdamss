/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davca.damss.damss.Service;

import com.davca.damss.damss.entity.Employe;
import com.davca.damss.damss.repository.EmployeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


/**
 *
 * @author isnov-pc
 */
@Service
public class EmployeRestService implements EmployeService {
    
    @Autowired
    private EmployeRepository employerepository;

    @Override
    public Employe createEmploye(Employe employe) {
       return employerepository.save(employe);
    }

    @Override
    public Employe getEmploye(Long id) {
        return employerepository.getOne(id);
    }

    @Override
    public Employe editEmploye(Employe employe) {
           return employerepository.save(employe);
    }

    @Override
    public void deleteEmploye(Employe employe) {
            employerepository.delete(employe);
    }

    @Override
    public void deleteEmploye(Long id) {
            employerepository.deleteById(id);
    }

    @Override
    public List<Employe> getAllEmploye(int pageNumber, int pageSiz) {
        return employerepository.findAll(new PageRequest(pageNumber, pageSiz)).getContent();
    }

    @Override
    public List<Employe> getAllEmploye() {
   return employerepository.findAll();
    }
    
    
}
